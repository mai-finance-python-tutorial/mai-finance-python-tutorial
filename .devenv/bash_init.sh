[[ -f .venv/Scripts/activate ]] && source .venv/Scripts/activate
[[ -f .venv/bin/activate ]] && source .venv/bin/activate

build() {
  poetry run mkdocs build --strict --verbose --site-dir public
}

start() {
  poetry run mkdocs serve --verbose
}
