# Prerequistes

This tutorial assumes you have some knowledge about programming and decentralized finance.

## Python

Python is a programming language which is well known for its lightweight syntax making it quite easy to learn.

If you are new to Python you can follow the following video tutorial on the Traversy Media Youtube Channel: [Python Crash Course For Beginners](https://www.youtube.com/watch?v=JJmcL1N2KQs)

If you don't know Python but any other programming languages then you should be able to follow the tutorial. Most of the things presented here can be directly implemented with Javascript because the main library we will use (web3.py) is a port of a Javascript library.

## Decentralized Finance

I will assume you already know how to interact with DeFi protocols deployed on the Polygon network. You need to know:

- How to use Metamask (or any other Web3 client)
- How to connect Metamask to the Polygon network
- Have funds on the Polygon network
- How to use [polygonscan](https://polygonscan.com/) to track your transaction history
- How to use well known protocols such as AAVE and Quickswap

You can find video tutorials about these subjects on Youtube. Be careful though because you will play with real funds in that space.

Here are two channels I recommand for general knowledge about DeFi and Yield Farming / Liquidity Providing:

- [Finematics](https://www.youtube.com/channel/UCh1ob28ceGdqohUnR7vBACA)
- [Taiki Maeda](https://www.youtube.com/user/TheTaikster)

If you are new to this space, keep in mind that it is risky for your money. Many protocols are not well tested, not audited, and even when they are they might still have unknown bugs or be exploited. Only put money you can afford to lose. And before learning how to code automated strategies, learn how to use these protocols manually.

## Code Editor

You will need a text editor and I recommend [VSCode](https://code.visualstudio.com/Download) on all operating systems. You can follow the following video tutorial to setup extensions in VSCode dedicated to Python: [Setting Up VSCode For Python Programming](https://www.youtube.com/watch?v=W--_EOzdTHk).
