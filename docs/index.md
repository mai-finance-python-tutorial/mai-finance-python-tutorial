# Introduction

*Disclaimer*: This tutorial is currently being written so what you are reading now is a draft.

This tutorial teaches you how to automate some actions on [Mai Finance](http://app.mai.finance) using Python.

More generally you will be able to apply the knownledge acquired here to any decentralized finance (DeFi) protocol. DeFi is really based on financial building blocks that you can combine the way you want. The only limit is your imagination for new strategies, and the time you spent searching for new projects to put in your toolbox. While most investment stategies can be executed manually, the power of programming offers you a way to avoid human mistakes, automate the boring stuff, optimize compound frequency, and remove human psychology biases from investment decisions.

This tutorial focuses on Mai Finance but we will also use AAVE to deposit and borrow tokens, as well as Quickswap for swapping tokens. In the future I plan to write other tutorials explaining how to combine many platforms to build more advanced strategies.
