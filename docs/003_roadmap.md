# Roadmap

Here is the (draft) roadmap of this tutorial:

- Setup the developer environment
    - Install Python
    - Install a code editor
    - Setup a virtual environment for the project
    - Install dependencies
    - Optional: create a dedicated Polygon account for the tutorial
    - Write a Web3.py Hello World
- Manage Mai Vaults from Python
    - From manual to code: how to reproduce manual Metamask interactions in code
    - Create a vault
    - Deposit MATIC in a vault
    - Withdraw MATIC from a vault
    - Borrow MAI from a vault
    - Repay MAI to a vault
    - Obtain measures for your vault (collateral value, collateral to debt ratio, MAI available to borrow)
    - Keeping your vault safe: borrow from AAVE to increase vault collateral
    - Delete a vault
- Qi farming with Python
    - Deposit Mai and USDC on Quickswap
    - Deposit LP token to Mai finance vault
    - Claim your rewards
    - Deposit Mai and Qi on Quickswap
    - Sell Qi for Mai on Quickswap
- Implement a complete farming strategy
    - Parameters of the strategy
    - Initial deposit to Mai+USDC
    - Each X seconds: claim Qi rewards from both pools
    - If vault health factor to low: borrow Matic from AAVE and deposit
    - Borrow Mai from vault
    - Deposit Mai+Qi to quickswap
    - Deposit LP tokens
- Deploy the code and run it on an AWS virtual machine
