# About Mai Finance

Mai Finance is a decentralized application deployed on the Polygon network. It implements a protocol to emit a USD pegged stablecoin named MAI (previously named miMATIC) from MATIC deposited in vaults by its users. It is similar to DAI which is emitted on the Ethereum network.

So if you are a MATIC holder and you don't want to sell your MATIC tokens, but you need stablecoins to do something else (buy more matic, farm another coin, pay you a salary, etc), you can use Mai Finance for this.

However it is not without risk. When you deposit MATIC in a vault you can borrow up to a certain percent of the deposited value in MAI. But if MATIC price goes down (and it can go down pretty fast), then you are subject to liquidation: another user can repay a part of your debt to acquire some of your MATIC at a lower price than the market price.

In this tutorial you will learn how to use python to automatically repay your debt or increase your MATIC collateral to avoid liquidation.

Another use case of is Qi farming. Qi is the gouvernance token of the protocol and a part of the supply is distributed to the community using various mechanisms. One of these is yield farming, a well known token distribution method from the DeFi ecosystem. You can earn Qi token by providing liquidity on two pools: USDC-Mai and Qi-Mai.

This tutorial will teach you how to automatically claim rewards and apply different strategies: sell rewards, borrow from your vault to pair with rewards and deposit back, sell half to deposit back, etc. Note that this part of the tutorial might become obsolete in a few months when Qi yield farming period ends. However most of the stuff explained here can be applied to other protocols offering yield farming opportunities.
